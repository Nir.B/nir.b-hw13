#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
//c-tor
Server::Server()
{
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

//d-tor
Server::~Server()
{
	try
	{
		
		closesocket(_serverSocket);
	}
	catch (...) {}
}

/*
function that set up the server
input: ip and port
output: none
*/
void Server::serve(int port, std::string ip)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); 
	sa.sin_family = AF_INET;   
	sa.sin_addr.s_addr = inet_addr(ip.c_str());    

	if (bind(this->_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

/*
function that accept client socket
input: none
output: none
*/
void Server::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	std::thread t = std::thread(&Server::clientHandler, this, client_socket);
	t.detach();
}

/*
function that handel the client
input: none
output: none
*/
void Server::clientHandler(SOCKET clientSocket)
{
	char* name = nullptr;
	char* name2 = nullptr;
	char* msg = nullptr;

	try
	{	
		while (true)
		{
			int code = Helper::getMessageTypeCode(clientSocket);
			if (code == MT_CLIENT_LOG_IN)
			{
				name = Helper::getPartFromSocket(clientSocket, std::stoi(Helper::getPartFromSocket(clientSocket, 2)));
				this->users[name] = &clientSocket;
				this->ms.insertUser(name);
				Helper::send_update_message_to_client(clientSocket, "", "", ms.getUsers());
			}
			else if (code == MT_CLIENT_UPDATE)
			{	
				name2 = Helper::getPartFromSocket(clientSocket, std::stoi(Helper::getPartFromSocket(clientSocket, 2)));
				msg = Helper::getPartFromSocket(clientSocket, std::stoi(Helper::getPartFromSocket(clientSocket, 5)));
				if (msg != "")
				{
					ms.sendMessages(name, name2, msg);
				}
				
				if (name2 != "" && this->users.find(name2) != this->users.end() && msg != "")
				{
					Helper::send_update_message_to_client(clientSocket, ms.getFile(name, name2), name2, ms.getUsers());
					Helper::send_update_message_to_client(*this->users[name2], ms.getFile(name, name2), name, ms.getUsers());
				}
				else
				{
					Helper::send_update_message_to_client(clientSocket, ms.getFile(name, name2), name2, ms.getUsers());
				}
			}
			
		}
		closesocket(clientSocket); 
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		this->ms.deleteUser(name);
		this->users.erase(name);
	}


}

