#pragma comment (lib, "ws2_32.lib")
#include <iostream>
#include <string>
#include <fstream>
#include "Helper.h"
#include "WSAInitializer.h"
#include "Server.h"

int main()
{
	std::ifstream config("config.txt");
	std::string conIp;
	std::string ip;
	std::string conPort;
	int port = 0;
	std::string temp;
	bool flag = false;

	if (config.is_open())
	{
		getline(config, conIp);
		getline(config, conPort);
	}

	for (int i = 0; i < conIp.size(); i++)
	{
		if (conIp[i] == '=')
		{
			flag = true;
		}
		else if (flag)
		{
			ip += conIp[i];
		}
	}
	flag = false;
	for (int i = 0; i < conPort.size(); i++)
	{
		if (conPort[i] == '=')
		{
			flag = true;
		}
		else if (flag)
		{
			temp += conPort[i];
		}
	}
	port = std::stoi(temp);
	
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(port, ip);
	}
	catch (std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
}