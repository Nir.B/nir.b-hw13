#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <thread>
#include <map>
#include "Helper.h"
#include "MessagesSender.h"


class Server
{
public:
	Server();
	~Server();
	void serve(int port, std::string ip);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
	std::map<std::string, SOCKET*> users;
	MessagesSender ms;
};

