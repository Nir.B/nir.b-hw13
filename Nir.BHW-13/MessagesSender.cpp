#include "MessagesSender.h"

// c-tor
MessagesSender::MessagesSender()
{
	this->flag = true;
}

/*
This function print the menu and manage the users
input: none
output: none
*/
void MessagesSender::menu()
{
	int choice = 0;
	bool flag = true;
	std::string username;
	while (flag)
	{
		std::cout << "1. Signin\n2. Signout\n3. Connected Users\n4.exit" << std::endl;
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			std::cout << "Enter your username: ";
			std::cin >> username;
			this->insertUser(username);
			break;
		case 2:
			std::cout << "Enter your username: ";
			std::cin >> username;
			this->deleteUser(username);
			break;
		case 3:
			this->printUsers();
			break;
		case 4:
			flag = false;
			this->flag = false;
			break;
		default:
			break;
		}
	}
	
}

/*
This function insert user to the vector
input: name of the user
output: none
*/
void MessagesSender::insertUser(std::string name)
{
	bool flag = false;
	for (int i = 0; i < this->users.size(); i++)
	{
		if (this->users[i] == name)
		{
			flag == true;
			break;
		}
	}

	if (!flag)
	{
		this->users.push_back(name);
		std::cout << "User added!" << std::endl;
	}
	else
	{
		std::cout << "User exists!" << std::endl;
	}
}

/*
This function delete user from the vector
input: name of the user
output: none
*/
void MessagesSender::deleteUser(std::string name)
{
	std::vector<std::string> temp;
	for (int i = 0; i < this->users.size(); i++)
	{
		if (this->users[i] != name)
		{
			temp.push_back(this->users[i]);
		}
		else
		{
			std::cout << "User delete!" << std::endl;
		}
	}
	this->users = temp;
}

/*
function that return all the users in theis format: <name>&<name>
input: none
output: string of all the users
*/
std::string MessagesSender::getUsers()
{
	std::string str;
	for (int i = 0; i < this->users.size(); i++)
	{
		if (i == 0)
		{
			str += this->users[i];
		}
		else
		{
			str += "&";
			str += this->users[i];
		}
	}
	return str;
}

/*
This function print the users
input: none
output: none
*/
void MessagesSender::printUsers()
{
	std::cout << "==============================================" << std::endl;
	for (int i = 0; i < this->users.size(); i++)
	{
		std::cout << (i+1) << ". " << this->users[i] << std::endl;
	}
	std::cout << "==============================================" << std::endl;
}

/*
function that return the file content
input: name and name2 (to make the path(<name>&<name>.txt)
output: string of the file content
*/
std::string MessagesSender::getFile(std::string name, std::string name2)
{
	std::string path;
	std::string file;
	std::string str[2];
	str[0] = name;
	str[1] = name2;
	std::sort(std::begin(str), std::end(str));
	path += str[0] + "&" + str[1] + ".txt";
	this->mtx.lock();
	/*** critical section start  ***/
	std::ifstream myfile(path);
	std::getline(myfile, file);
	myfile.close();
	/*** critical section end  ***/
	this->mtx.unlock();
	return file;
}


/*
This function send the mesages to the users
input: none
output: none
*/
void MessagesSender::sendMessages(std::string name, std::string name2, std::string msg)
{
	std::string str[2];
	std::string path;
	std::string fullMsg;
	std::ofstream myfile;
	str[0] = name;
	str[1] = name2;
	std::sort(std::begin(str), std::end(str));
	path += str[0] + "&" + str[1] + ".txt";
	
	this->mtx.lock();
	/*** critical section start  ***/
	myfile.open(path, std::ios::app);
	if (myfile.is_open())
	{
		fullMsg += "&MAGSH_MESSAGE&&Author&" + name + "&DATA&" + msg;
		myfile << fullMsg;
	}
	myfile.close();
	/*** critical section end  ***/
	this->mtx.unlock();
}
