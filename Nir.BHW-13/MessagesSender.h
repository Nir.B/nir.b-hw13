#pragma once
#include <iostream>
#include <vector>
#include <queue>
#include <thread>
#include <fstream>
#include <string>
#include <mutex>
#include <streambuf>
#include <chrono>

class MessagesSender
{
private:
	std::vector<std::string> users;
	std::queue<std::string> messages;
	bool flag;
	std::mutex mtx;

public:
	MessagesSender();
	void menu();
	void insertUser(std::string name);
	void deleteUser(std::string name);
	std::string getUsers();
	void printUsers();
	std::string getFile(std::string name, std::string name2);
	void sendMessages(std::string name, std::string name2, std::string msg);
};

